# Mandiri Official API

Untuk menggunakan dan melakukan uji sandbox pada official API bank Mandiri, pertama kali kita diperlukan untuk memiliki akun pada https://developer.bankmandiri.co.id/

## Register Akun

Step by step

* Pada button My account di pojok kanan atas di halaman landing https://developer.bankmandiri.co.id/, klik Register


![image](images/dropdown-account.png)


* Isi data yang dibutuhkan pada form registrasi yang muncul


![image](images/form-register.png)

* Setelah sukses melakukan registrasi, kita bisa langsung login dengan akun yang telah kita buat
* Setelah sukses login, maka di pojok kanan atas akan muncu icon user, ini menandakan kita sudah sukses login


![image](images/dropdown-user.png)

* Pada menu Discover API, Klik menu Api Gallery, untuk melihat list api yang tersedia untuk kita gunakan


![image](images/dropdown-api.png)


* Klik explore docs pada salah satu api (untuk saat ini kita akan menggunakan balance inquiry saving account dan transaction inquiry saving account)

![image](images/page-api-list.png)

* Ini adalah halaman dokumentasi API mandiri

![image](images/page-api.png)

* Untuk pertama kali akses, kita perlu membuat akses Application, tata cara pembuatan application ini terdapat pada halaman dokumentasi API di atas

![image](images/notif-try-api.png)

* Kita perlu klik Try API pada menu Balance inquiry saving account
* Pada saat pertama kali halaman tersebut di buka, akan ada notifikasi seperti di bawah, yang menandakan kita belum membuat hak aksesnya, klik link Click here

![image](images/notif-create-application.png)

* Lengkapi data yang diperlukan


![image](images/form-request-token.png)

* Setelah selesai buka menu application yang terdapat pada icon user kita yg berada di pojok kanan atas, list application yang telah kita buat akan muncul di halaman ini

![image](images/info-application.png)

* Buka application yg telah terdaftar
* Pada halaman detail application, kita dapat menemukan credentials oauth, yang nantinya akan kita gunakan untuk generate access token


![image](images/info-credentials.png)


## Generate Signature

Signature pada api mandiri dibagi menjadi 2, signature yang diperuntukan hanya untuk authentication atau proses mendapatkan token dan signature yang digunakan untuk endpoint transaksi

### Authentication Signature

```php
<?php

$dateCurrent = (new \DateTime)->format('Y-m-d\TH:i:s.v\T+0700');

// Specify private key location, passphrase, data, and hash algorithm
$privateKeyPath = 'API_Portal.pem';
$password = 'mandiri123';
$clientId = '';
$data = $clientId.'|'.$dateCurrent;
$rsaAlgorithm = OPENSSL_ALGO_SHA256;

// Load private key file
$fp = fopen($privateKeyPath, 'r');
$privatekeyFile = fread($fp, 8192);
fclose($fp);
$privatekey = openssl_pkey_get_private($privatekeyFile, $password);

// Sign data
echo 'data: '.$data.PHP_EOL;
openssl_sign($data, $signature, $privatekey, $rsaAlgorithm);
echo 'signature: '.base64_encode($signature).PHP_EOL;
```

Di atas merupakan script php native yang dapat digunakan untuk menghasilkan signature untuk authentication
Silahkan isi $clientId dengan clientId pada halaman application di atas

Simpan script di atas dengan nama `signature.php` atau lainnya, lalu run melalui terminal dengan perintah

```
php signature.php
```

Perintah di atas akan menghasilkan signature dan timestamp
timestamp dapat diambil setelah tanda pipe "|"

> `API_Portal.pem` dapat di download di halaman dokumentasi api

Transaction Signature

```php
<?php

$http_method = 'POST';
$url_path = '/openapi/transactions/v1.0/todayHistoryInquiry/casa';
$access_token = '';
$jsonBodyRequest = [
    'accountNo' => '1190080001041',
    'postingTime' => '13:00:00.0Z'
];
$timestamp = (new \DateTime)->format('Y-m-d\TH:i:s.v\T+0700');

$string = $http_method.':'.$url_path.':'.$access_token.':'.json_encode($jsonBodyRequest).':'.$timestamp;
$clientSecret = '';

$hash = hash_hmac('sha512', $string, $clientSecret);

echo 'Token: '.$access_token.PHP_EOL;
echo 'Timestamp: '.$timestamp.PHP_EOL;
echo 'Hash: '.$hash.PHP_EOL;
```

### Get Access Token

![image](images/postman-token-header.png)

Masukan timestamp yang di dapat dari generate signature untuk authentication
Masukan juga signature yang telah di generate
Bila data sudah benar, maka response yang di dapat adalah access token dan jangka waktu access token tersebut

### Request Get Current Balance

Generate signature untuk transakasi, sesuai kan `$jsonBodyRequest` dengan request yang di kirim ke api mandiri, dan perlu dipastikan data pada `$jsonBodyRequest` perlu sama dengan yang di kirim ke API

![image](images/postman-inquiry-header.png)

Sama seperti sebelumnya timestamp dan signature di isi dari data yg di generate sebelum ini,

Bila parameter telah benar maka response akan memunculkan nilai saldo yang tersedia
