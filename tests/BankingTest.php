<?php

use PHPUnit\Framework\TestCase;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class BankingTest extends TestCase
{
    protected $moobank;
    protected $moobankMandiri;

    public function setUp(): void
    {
        parent::setUp();
        $this->moobank = new Moobank\Moobank;
        $this->moobankMandiri = $this->moobank->create(\Moobank\MandiriApi\Factory::class, new Client);

        // Set parameters
        $this->moobankMandiri->setParameter('credentials', [
            'clientId' => '333a4a77-fee2-41e1-af69-32b8290c2ea9',
            'clientSecret' => 'f1b1d987-4557-457a-bee7-9cbb1f5f7cc3',
            'password' => 'mandiri123',
        ]);

        // $this->moobankMandiri->setParameter('debug', true);
    }

    public function testCallBankingObject()
    {
        $banking = $this->moobankMandiri->banking;

        $this->assertInstanceOf(Moobank\MandiriApi\Banking::class, $banking);
    }

    public function testCallBankingGetToken()
    {
        $banking = $this->moobankMandiri->banking;

        $this->assertInstanceOf(Moobank\MandiriApi\Banking::class, $banking);

        $accessToken = $this->moobankMandiri->getToken();

        $this->assertTrue(is_string($accessToken) && strlen($accessToken) > 0);
    }

    public function testCallInquiry()
    {
        $this->moobankMandiri->setParameter('partnerId', 'UAT001');
        $this->moobankMandiri->setParameter('externalId', '501001');

        $inquiries = $this->moobankMandiri->banking->inquiry([
            'accountNo' => '1190080001041',
            'postingTime' => '13:05:00.0Z',
        ])->send();

        $this->assertInstanceOf(Moobank\MandiriApi\Message\BankingInquiryResponse::class, $inquiries->getResponse());

        $this->assertObjectHasAttribute('date', $inquiries->getResponse()->getData()[0]);
    }

    public function testCallBalance()
    {
        $this->moobankMandiri->setParameter('partnerId', 'UAT001');
        $this->moobankMandiri->setParameter('externalId', '501001');

        $accountNo = 1190080001041;

        $balance = $this->moobankMandiri->banking->balance([
            'accountNo' => $accountNo
        ])->send();

        $this->assertInstanceOf(Moobank\MandiriApi\Message\BankingBalanceResponse::class, $balance->getResponse());

        $this->assertGreaterThan(0, $balance->getResponse()->getData()[$accountNo]->balance['available']);
    }
}
