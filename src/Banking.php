<?php

namespace Moobank\MandiriApi;

use Moobank\AbstractGateway;
use Psr\Http\Client\ClientInterface;
use Moobank\Message\RequestInterface;

class Banking extends AbstractGateway
{
    public function __construct(ClientInterface $httpClient = null, RequestInterface $httpRequest = null)
    {
        $this->httpClient = $httpClient;
        $this->httpRequest = $httpRequest;
    }

    public function __get($property)
    {
        # code...
    }

    public function getName()
    {
        return 'Mandiri Official API - Banking Service';
    }

    public function getModuleName()
    {
        return 'service.api.official.mandiri';
    }

    public function inquiry(array $parameters = [])
    {
        $data = $this->createRequest(\Moobank\MandiriApi\Message\BankingInquiryRequest::class, $parameters);
        return $data;
    }

    public function balance(array $parameters = [])
    {
        $data = $this->createRequest(\Moobank\MandiriApi\Message\BankingBalanceRequest::class, $parameters);
        return $data;
    }
}