<?php

namespace Moobank\MandiriApi\Message;

use Moobank\Entities\Statement;
use Moobank\Message\AbstractResponse;

class BankingInquiryResponse extends AbstractResponse
{
    public function getData()
    {
        if (isset($this->data) && isset($this->data->data)) {
            $data = [];
            foreach ($this->data->data as $item) {
                $data[] = new Statement([
                    'date' => $item->postingDate.' '.$item->postingTime,
                    'branch' => '',
                    'entry' => $item->debitCreditCode == 'C' ? 'Credit' : 'Debit',
                    'amount' => $item->transactionAmount,
                    'description' => $item->transactionDescription,
                    'hash' => md5(json_encode($item)),
                ]);
            }
            return $data;
        }

        return parent::getData();
    }
}