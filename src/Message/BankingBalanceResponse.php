<?php

namespace Moobank\MandiriApi\Message;

use Moobank\Entities\Balance;
use Moobank\Message\AbstractResponse;

class BankingBalanceResponse extends AbstractResponse
{
    public function getData()
    {
        if ($this->data
            && isset($this->data->responseCode)
            && $this->data->responseCode == 2000000
        ) {
            $data = $this->data->data;
            return [
                $data->accountNo => new Balance([
                    'balance' => [
                        'available' => $data->availableBalance,
                        'hold' => $data->holdAmount,
                        'float' => 0,
                    ]
                ])
            ];
        }

        return $this->data;
    }
}