<?php

namespace Moobank\MandiriApi\Message;

use Moobank\Message\AbstractRequest;

class AccessTokenRequest extends AbstractRequest
{
    protected $endpoint = 'https://api.bankmandiri.co.id/openapi/auth/token';
    protected $sandboxEndpoint = 'https://sandbox.bankmandiri.co.id/openapi/auth/token';

    protected $method = 'POST';

    public function getHeaders()
    {
        $headers = parent::getHeaders();

        $timestamp = $this->parameters->get('timestamp', date('Y-m-d H:i:s'));
        if (! $timestamp instanceof \DateTimeInterface) {
            $timestamp = new \DateTime($timestamp);
        }

        $signature = (new PopulateSignature([
            'privateKeyPath' => __DIR__.'/../API_Portal.pem',
            'password' => $this->parameters->get('credentials')['password']
            ]))
            ->forAuth($this->parameters->get('credentials')['clientId'], $timestamp);

        return array_merge($headers, [
            'X-Mandiri-Key' => $this->parameters->get('credentials')['clientId'],
            'X-TIMESTAMP' => $timestamp->format('Y-m-d\TH:i:s.v\T+0700'),
            'X-SIGNATURE' => $signature,
        ]);
    }

    public function getData()
    {
        return [
            'grant_type' => 'client_credentials',
        ];
    }

    public function createResponse($response)
    {
        if (false === ($response instanceof \Psr\Http\Message\ResponseInterface)) {
            $response = new \Moobank\MandiriApi\Message\AccessTokenResponse($this, $response);
        }

        $this->response = $response;

        return $this;
    }
}