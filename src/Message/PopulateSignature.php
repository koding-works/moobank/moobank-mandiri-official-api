<?php

namespace Moobank\MandiriApi\Message;

class PopulateSignature
{
    protected $configuration;

    public function __construct($configuration = [])
    {
        $this->configuration = $configuration;
    }

    public function forAuth(string $clientId, \DateTimeInterface $date)
    {
        $privateKeyPath = $this->configuration['privateKeyPath'];
        $password = $this->configuration['password'];
        $data = sprintf('%1s|%2s', $clientId, $date->format('Y-m-d\TH:i:s.v\T+0700'));
        $rsaAlgorithm = OPENSSL_ALGO_SHA256;

        // load private key
        $fp = fopen($privateKeyPath, 'r');
        $privatekeyFile = fread($fp, 8192);
        fclose($fp);

        $privatekey = openssl_pkey_get_private($privatekeyFile, $password);

        openssl_sign($data, $signature, $privatekey, $rsaAlgorithm);

        return base64_encode($signature);
    }

    public function forTransaction(string $clientSecret, string $httpMethod, string $uriPath, string $accessToken, array $body = [], \DateTimeInterface $date = null)
    {
        if (is_null($date)) {
            $date = new \DateTime;
        }

        $timestamp = $date->format('Y-m-d\TH:i:s.v\T+0700');
        $string = $httpMethod.':'.$uriPath.':'.$accessToken.':'.json_encode($body).':'.$timestamp;

        $signature = hash_hmac('sha512', $string, $clientSecret);

        return $signature;
    }
}