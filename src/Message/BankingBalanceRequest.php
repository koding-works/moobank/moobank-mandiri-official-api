<?php

namespace Moobank\MandiriApi\Message;

use Moobank\Message\AbstractRequest;

class BankingBalanceRequest extends AbstractRequest
{
    protected $endpoint = 'https://api.bankmandiri.co.id/openapi/customers/v1.0/balanceInquiry/casa';
    protected $sandboxEndpoint = 'https://sandbox.bankmandiri.co.id/openapi/customers/v1.0/balanceInquiry/casa';

    protected $method = 'POST';

    public function getHeaders()
    {
        $headers = parent::getHeaders();

        $timestamp = $this->parameters->get('timestamp', new \DateTime);
        if (! $timestamp instanceof \DateTimeInterface) {
            $timestamp = new \DateTime($timestamp);
        }

        $parsedUrl = parse_url($this->getEndpoint());
        $uriPath = (isset($parsedUrl['path']) ? $parsedUrl['path'] : '');

        $signature = (new PopulateSignature([
            'privateKeyPath' => '../API_Portal.pem',
            'password' => $this->parameters->get('credentials')['password']
            ]))
            ->forTransaction($this->parameters->get('credentials')['clientSecret'], $this->getMethod(), $uriPath, $this->parameters->get('accessToken'), $this->getData(), $timestamp);

        return array_merge($headers, [
            'Authorization' => 'Bearer '.$this->parameters->get('accessToken'),
            'X-PARTNER-ID' => $this->parameters->get('partnerId'),
            'X-EXTERNAL-ID' => $this->parameters->get('externalId'),
            'X-TIMESTAMP' => $timestamp->format('Y-m-d\TH:i:s.v\T+0700'),
            'X-SIGNATURE' => $signature,
        ]);
    }

    public function getData()
    {
        return [
            'accountNo' => (string) $this->parameters->get('accountNo'),
        ];
    }

    public function createResponse($response)
    {
        if (false === ($response instanceof \Psr\Http\Message\ResponseInterface)) {
            $response = new \Moobank\MandiriApi\Message\BankingBalanceResponse($this, $response);
        }

        $this->response = $response;

        return $this;
    }
}