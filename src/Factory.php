<?php

namespace Moobank\MandiriApi;

date_default_timezone_set('Asia/Jakarta');

use Moobank\AbstractGateway;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;

class Factory extends AbstractGateway
{
    protected $accessToken;

    public function __construct(ClientInterface $httpClient = null, RequestInterface $httpRequest = null)
    {
        parent::__construct($httpClient, $httpRequest);
    }

    public function __get($property)
    {
        $className = sprintf('%1s\%2s', 'Moobank\MandiriApi', ucfirst(strtolower($property)));
        if (class_exists($className)) {

            $class  = new $className($this->httpClient, $this->httpRequest);
            $class->initialize($this->parameters->all());
            $class->setParameter('accessToken', $this->getToken());

            return $class;
        }

        throw new \Moobank\Exception\ClassNotFoundException;
    }

    public function getName()
    {
        return 'Mandiri Official API';
    }

    public function getModuleName()
    {
        return 'moobank.api.official.mandiri';
    }

    public function getToken()
    {
        if (! $this->accessToken) {
            $token = $this->createRequest(\Moobank\MandiriApi\Message\AccessTokenRequest::class)->send();

            try {
                $response = $token->getResponse();
                $data = $response->getData();
                $this->accessToken = $data->accessToken;
            } catch (\Exception $e) {}
        }

        return $this->accessToken;
    }
}