# Moobank - Mandiri Official API

This package is part of moobank package

## TOC

- [Requirement](#requirement)
- [Installation](#installation)
- [Basic Usage](#basic-usage)

## Requirement

Here are the required requirement

- psr/http-message: ^1.0
- psr/http-client: ^1.0
- guzzlehttp/guzzle: ^7.1
- symfony/http-foundation: ^5.1
- symfony/psr-http-message-bridge: ^2.0.2
- moobank/moobank @dev

## Installation

> Because moobank is on private repository, so we need setup additional configuration on out composer to installing it

Put this on your composer.json
```json
{
    "require": [
        "moobank/mandiri-official-api": "@dev",
        ...
    ],
    "repositories": [
        {
            "type": "git",
            "url": "https://gitlab.com/koding-works/moobank/moobank-mandiri-official-api.git"
        }
    ],
    ...
}
```

then do composer update from your terminal

```
composer update
```

<<<<<<< HEAD
> Use must install [moobank](https://gitlab.com/koding-works/moobank/moobank) core library first before installing this plugin

## Example

Example usage of moobank - mandiri api library
=======
## Basic Usage

Basic usage of moobank - mandiri api library
```

## Usage

### Initialize

To init the plugin object, we must init moobank core library first

```php
$moobank = new Moobank\Moobank;
```

then we register the plugin factory into moobank core library

we must register http client on the second params, we use guzzle here, because its a powerfull curl library on php nowdays

```php
$mandiri = $moobank->create(\Moobank\MandiriApi\Factory::class, new \GuzzleHttp\Client)
```

### Configure the credentials

Here is required configuration to use mandiri api

```php
$mandiri->setParameter('credentials', [
    'clientId' => '',
    'clientSecret' => '',
    'password' => '',
]);
```

### Get current balance

To get current balance, just follow command below

This command return available, hold, etc amount on balance object

```php

$bca->setParameter('corporateId', 'BCAAPI2016');

$accountNo = '';
$balance = $bca->banking->balance([
    'accountNo' => $accountNo
])->send();

var_dump($balance->getResponse()->getData()[$accountNo]->balance['available']);

```

### Get statements

To get current statements, follow command below

This command returns a collections of statement array

```php

$bca->setParameter('corporateId', 'BCAAPI2016');

$inquiries = $this->moobankBca->banking->inquiry([
    'accountNo' => '',
    'dateStart' => '2016-08-29',
    'dateEnd' => '2016-09-01'
])->send();

var_dump($balance->getResponse()->getData());

=======
```
